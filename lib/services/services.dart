import 'package:http/http.dart' as http;

class Service {
  static Future<http.Response> getCars() async {
    return http.get("https://gitlab.com/flutterprojects4/carrentalapp/-/raw/master/resources/network/cars.json");
  }
}