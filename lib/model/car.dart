class Car {
  String nome;
  String combustivel;
  String preco;
  String motor;
  String cambio;
  String lugares;
  String imageURL;

  Car({this.nome, this.combustivel, this.preco, this.motor, this.cambio, this.lugares, this.imageURL});

  factory Car.fromJson(Map<String, dynamic> json) {
    return _$CarFromJson(json);
  }
}

Car _$CarFromJson(Map<String, dynamic> json) {
  return Car(
    nome: json['nome'] as String,
    combustivel: json['combustivel'] as String,
    preco: json['preco'] as String,
    motor: json['motor'] as String,
    cambio: json['cambio'] as String,
    lugares: json['lugares'] as String,
    imageURL: json['imageURL'] as String
  );
}