import 'package:CarRentalApp/common/app_colors/app_colors.dart';
import 'package:CarRentalApp/common/components/bottom_navigation/bottom_navigation.dart';
import 'package:CarRentalApp/features/cars_page/cars_page.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

// ignore: must_be_immutable
class HomeTab extends StatefulWidget {
  
@override
  _HomeTabState createState() => _HomeTabState();
}

class _HomeTabState extends State<HomeTab> {

  int _currentIndex = 0;
  final List<Widget> _children = [
    CarsPage(),
    CarsPage(),
    CarsPage(),
    CarsPage()
  ];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      floatingActionButton: FloatingActionButton(
        onPressed: () {},
        child: Container(
          width: 60,
          height: 60,
          child: Icon(Icons.add, size: 40),
          decoration: BoxDecoration(
              shape: BoxShape.circle,
              gradient: LinearGradient(
                  begin: Alignment.topLeft,
                  end: Alignment.topRight,
                  colors: [
                    AppColors.blueAppColor,
                    AppColors.blueDarkAppColor
                  ])),
        ),
      ),
      bottomNavigationBar: BottomNavigationWidget(pageIndex: _currentIndex, callback: myCallback),
      floatingActionButtonLocation: FloatingActionButtonLocation.centerDocked,
      body: Stack(
        children: <Widget>[
          _children[_currentIndex],
        ],
      ),
    );
  }

  myCallback(int index){
    setState(() {
      // ignore: unnecessary_statements
      _currentIndex = index;
    });
  }
}
