import 'package:CarRentalApp/common/app_colors/app_colors.dart';
import 'package:CarRentalApp/common/components/background/default_background.dart';
import 'package:CarRentalApp/common/components/default_navigation/default_navigation.dart';
import 'package:CarRentalApp/common/styles/styles.dart';
import 'package:CarRentalApp/model/car.dart';
import 'package:flutter/material.dart';

class CarDetailPage extends StatelessWidget {
  final Car carro;
  const CarDetailPage({this.carro});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Stack(
        children: [
          DefaultBackground(),
          Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              DefaultNavigation(text: "Car Detail"),
              Container(                
                child: Padding(
                padding: EdgeInsets.symmetric(horizontal: 20),
                child: Container(
                  margin: EdgeInsets.symmetric(vertical: 10, horizontal: 4),
                  decoration: BoxDecoration(
                    color: AppColors.whiteAppColor,
                    borderRadius: BorderRadius.all(Radius.circular(20)),
                    boxShadow: [
                      BoxShadow(
                        color: Colors.grey.withOpacity(0.3),
                        spreadRadius: 2,
                        blurRadius: 3,
                        offset: Offset(0, 5), // changes position of shadow
                      ),
                    ],
                  ),
                  child: Column(
                    children: [
                      Container(
                        child: Padding(
                          padding: EdgeInsets.symmetric(
                              vertical: 20, horizontal: 20),
                          child: Row(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                children: [
                                  Text(
                                    carro.nome,
                                    style: Styles.carTitle,
                                  ),
                                  SizedBox(height: 5),
                                  Text(
                                    carro.combustivel,
                                    style: Styles.carSubtitle,
                                  ),
                                  SizedBox(height: 10),
                                  Text(
                                    carro.preco,
                                    style: Styles.carPrice,
                                  ),
                                  SizedBox(height: 20),
                                  Row(
                                    children: [
                                      Image.asset("resources/images/verified.png"),
                                      Text(
                                        " Safe Vehicle",
                                        style: Styles.safeVehicle,
                                      )
                                    ],
                                  ),
                                ],
                              ),
                              Column(            
                                crossAxisAlignment: CrossAxisAlignment.end,                
                                children: [
                                  Image.asset("resources/images/favourite.png")
                                ],
                              )
                            ],
                          ),
                        ),
                      ),
                      Container(
                        child: Column(
                          children: [
                            Container(
                              height: 160,
                              width: 275,
                              child: Image.network(carro.imageURL),
                            )
                          ],
                        ),
                      ),
                      Container(
                        child: Row(
                          crossAxisAlignment: CrossAxisAlignment.center,
                          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                          children: [
                            Image(                              
                              image: AssetImage("resources/images/engine.png")
                            ),
                            Text(
                              carro.motor,
                              style: Styles.carInfo,
                            ),
                            Image(                              
                              image: AssetImage("resources/images/gear.png")
                            ),
                            Text(
                              carro.cambio,
                              style: Styles.carInfo,
                            ),
                            Image(                              
                              image: AssetImage("resources/images/seats.png")
                            ),
                            Text(
                              carro.lugares,
                              style: Styles.carInfo,
                            ),
                          ],
                        ),
                      )
                    ],
                  ),
                ),
              ))
            ],
          )
        ],
      ),
    );
  }
}
