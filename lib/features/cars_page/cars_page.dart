import 'dart:convert';
import 'package:CarRentalApp/common/app_colors/app_colors.dart';
import 'package:CarRentalApp/common/components/background/default_background.dart';
import 'package:CarRentalApp/common/components/default_navigation/default_navigation.dart';
import 'package:CarRentalApp/common/styles/styles.dart';
import 'package:CarRentalApp/features/car_detail_page/car_detail_page.dart';
import 'package:CarRentalApp/model/car.dart';
import 'package:CarRentalApp/services/services.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class CarsPage extends StatefulWidget {
  @override
  _CarsPageState createState() => _CarsPageState();
}

class _CarsPageState extends State<CarsPage> {
  bool _isLoading = false;
  List<Car> carros = [];

  @override
  void initState() {
    super.initState();
    _fetchCarros();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Stack(
        children: [
          DefaultBackground(),
          Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              DefaultNavigation(text: "Sports Car"),
              carros.isEmpty
                  ? SizedBox.shrink()
                  : Expanded(
                      child: Padding(
                      padding: EdgeInsets.symmetric(horizontal: 20),
                      child: ListView.builder(
                          itemCount: carros.length,
                          itemBuilder: (context, index) {
                            return CarListItem(
                              carro: carros[index],
                              callback: (){
                                Navigator.of(context).push(MaterialPageRoute(
                                  builder: (context) => CarDetailPage(
                                    carro: carros[index],
                                  )));
                              },
                            );
                          }),
                      )
                    ),
                _isLoading 
                ? Center(child: CupertinoActivityIndicator())
                : SizedBox.shrink()
            ],
          )
        ],
      ),
    );
  }

  _fetchCarros() {
    setState(() {
      _isLoading = true;
    });

    Service.getCars().then((response) {
      var json = jsonDecode(response.body)["cars"];

      carros = [];
      for (var carJson in json) {
        carros.add(Car.fromJson(carJson));
      }

      setState(() {
        _isLoading = false;
      });
    });
  }
}

class CarListItem extends StatelessWidget {
  final Car carro;
  final Function callback;

  const CarListItem({this.carro, this.callback});

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
        onTap: callback,
        child: Container(
        margin: EdgeInsets.symmetric(vertical: 10, horizontal: 4),
        decoration: BoxDecoration(
          color: AppColors.whiteAppColor,
          borderRadius: BorderRadius.all(Radius.circular(20)),
          boxShadow: [
            BoxShadow(
              color: Colors.grey.withOpacity(0.3),
              spreadRadius: 2,
              blurRadius: 3,
              offset: Offset(0, 5), // changes position of shadow
            ),
          ],
        ),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Container(
              child: Padding(
                padding: EdgeInsets.symmetric(vertical: 20, horizontal: 10),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Text(
                      carro.nome,
                      style: Styles.carTitle,
                    ),
                    SizedBox(height: 5),
                    Text(
                      carro.combustivel,
                      style: Styles.carSubtitle,
                    ),
                    SizedBox(height: 10),
                    Text(
                      carro.preco,
                      style: Styles.carPrice,
                    ),
                    SizedBox(height: 20),
                    Row(
                      children: [
                        Image.asset("resources/images/verified.png"),
                        Text(
                          "  Safe Vehicle",
                          style: Styles.safeVehicle,
                        )
                      ],
                    ),
                  ],
                ),
              ),
            ),
            Container(
              child: Padding(
                padding: EdgeInsets.symmetric(vertical: 20, horizontal: 5),
                child: Column(
                  children: [
                    Container(
                      height: 90,
                      width: 180,
                      child: Image.network(carro.imageURL),
                    )
                  ],
                ),
              ),
            )
          ],
        ),
      ),
    );
  }
}
