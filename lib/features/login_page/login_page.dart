import 'package:CarRentalApp/common/app_colors/app_colors.dart';
import 'package:CarRentalApp/common/components/background/default_background.dart';
import 'package:CarRentalApp/common/styles/styles.dart';
import 'package:CarRentalApp/features/home_tab/home_tab.dart';
import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';

class LoginPage extends StatelessWidget {
  const LoginPage({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        child: Stack(
          children: [
            DefaultBackground(
              color: AppColors.backgroundAppColor,
            ),
            SafeArea(
              child: Container(
                  padding: EdgeInsetsDirectional.only(start: 90),
                  child: Image.asset("resources/images/login_background.png")),
            ),
            Container(
              padding: EdgeInsets.only(left: 30, top: 250),
              child: Image.asset("resources/images/logo.png"),
            ),
            Positioned(
              bottom: 60,
              child: SafeArea(
                child: Container(
                    width: MediaQuery.of(context).size.width,
                    padding: EdgeInsets.only(left: 20, right: 20),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        CustomHeader(
                          title: "Car Rental,",
                          style: Styles.firstHeaderText
                        ),
                        CustomHeader(
                          title: "Travel, Love a car.",
                          style: Styles.secondHeaderText
                        ),
                        SizedBox(height: 40),
                        CustomTextField(placeholder: "E-mail adress"),
                        SizedBox(height: 20),
                        CustomTextField(placeholder: "Password", isPassword: true),
                        SizedBox(height: 30),
                        LoginButton(
                          title: "LOGIN ME",
                          callback: (){
                            navigateToCarsPage(context: context);
                          },
                          ),
                        SizedBox(height: 40),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            Text("Don't have a account? ",
                              style: Styles.secondButtonText
                              ),
                            Text("Sign up",
                              style: Styles.thirdButtonText
                              ),    
                          ],
                        )
                      ],
                    )
                  ),
              ),
            )
          ],
        ),
      ),
    );
  }
                            
  navigateToCarsPage({BuildContext context}) {
    Navigator.of(context).push(MaterialPageRoute(builder: (context) => HomeTab()));
  }
}

class LoginButton extends StatelessWidget {
  final String title;
  final Function callback;

  const LoginButton({this.title, this.callback});

  @override
  Widget build(BuildContext context) {
    return Container(
      width: double.infinity,
      height: 70,
      decoration: BoxDecoration(
        borderRadius: BorderRadius.all(Radius.circular(10)),
        gradient: LinearGradient(
          begin: Alignment.topLeft,
          end: Alignment.topRight,
          colors: [AppColors.blueAppColor, AppColors.blueDarkAppColor ]
        )
      ),
      child: CupertinoButton(
        onPressed: callback,
        child: Text(
          title,
          style: Styles.firstButtonText
          ),
      ),
    );
  }
}

class CustomTextField extends StatelessWidget {
  final String placeholder;
  final bool isPassword;
  const CustomTextField({this.placeholder, this.isPassword = false});

  @override
  Widget build(BuildContext context) {
    return Container(
      width: double.infinity,
      child: TextField(
        obscureText: isPassword,
        style: TextStyle(
          height: 1.5
        ),
        decoration: InputDecoration(
          fillColor: AppColors.textGrayAppColor,
          border: OutlineInputBorder(
            borderRadius: BorderRadius.all(Radius.circular(10))
          ),
          labelText: placeholder,
        ),
      ),
    );
  }
}

class CustomHeader extends StatelessWidget {
  final String title;
  final TextStyle style;

  const CustomHeader({this.title, this.style});

  @override
  Widget build(BuildContext context) {
    return Row(
      children: [
        Text(title, style: style),
      ],
    );
  }
}
