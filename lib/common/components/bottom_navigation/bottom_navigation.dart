import 'package:CarRentalApp/common/app_colors/app_colors.dart';
import 'package:flutter/material.dart';

// ignore: must_be_immutable
class BottomNavigationWidget extends StatefulWidget {
  int pageIndex = 0;
  final Function callback;

  BottomNavigationWidget({this.pageIndex, this.callback});

  @override
  _BottomNavigationWidgetState createState() => _BottomNavigationWidgetState();
}

class _BottomNavigationWidgetState extends State<BottomNavigationWidget> {

  @override
  Widget build(BuildContext context) {
    return Container(
        decoration: BoxDecoration(
            boxShadow: [
              BoxShadow(
                color: Colors.grey.withOpacity(0.5),
                spreadRadius: 10,
                blurRadius: 20,
                offset: Offset(0, 5), // changes position of shadow
              ),
            ],
        ),
        child: ClipRRect(                            
          borderRadius: BorderRadius.only(
            topRight: Radius.circular(40),
            topLeft: Radius.circular(40),
          ),
          child: BottomNavigationBar(
            type: BottomNavigationBarType.fixed,
            onTap: onTabTapped,
            currentIndex: widget.pageIndex,                
            items: [
              BottomNavigationBarItem(                  
                  title: Text("Homepage"),                        
                  icon:
                  ImageIcon(
                    AssetImage("resources/images/home.png"),
                    color: AppColors.textBlackAppColor,
                  ),
                  activeIcon:
                  ImageIcon(
                    AssetImage("resources/images/home.png"),
                    color: AppColors.blueAppColor,
                  ),
              ),
              BottomNavigationBarItem(
                  title: Text("Works"),
                  icon: 
                  ImageIcon(
                    AssetImage("resources/images/works.png"),
                    color: AppColors.textBlackAppColor,
                  ),
                  activeIcon: 
                  ImageIcon(
                    AssetImage("resources/images/works.png"),
                    color: AppColors.blueAppColor,
                  ),
              ),
              BottomNavigationBarItem(
                  title: Text("Calendar"),
                  icon: 
                  ImageIcon(
                    AssetImage("resources/images/calendar.png"),
                    color: AppColors.textBlackAppColor,
                  ),
                  activeIcon: 
                  ImageIcon(
                    AssetImage("resources/images/calendar.png"),
                    color: AppColors.blueAppColor,
                  ),
              ),
              BottomNavigationBarItem(
                  title: Text("Profile"),
                  icon: 
                  ImageIcon(
                    AssetImage("resources/images/profile.png"),
                    color: AppColors.textBlackAppColor,
                  ),
                  activeIcon: 
                  ImageIcon(
                    AssetImage("resources/images/profile.png"),
                    color: AppColors.blueAppColor,
                  ),
              )
            ],
            unselectedItemColor: AppColors.textBlackAppColor,
            selectedItemColor: AppColors.blueAppColor,
            showUnselectedLabels: true,
          ),
        ),
      );    
  }

  void onTabTapped(int index) {
      setState(() {
        widget.pageIndex = index;
        widget.callback(widget.pageIndex);
      });
    }
}