import 'package:CarRentalApp/common/app_colors/app_colors.dart';
import 'package:CarRentalApp/common/styles/styles.dart';
import 'package:flutter/material.dart';

class DefaultNavigation extends StatelessWidget {
  final String text;

  const DefaultNavigation({this.text});

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.only(top: 40),
      width: double.infinity,
      height: 100,
      decoration: BoxDecoration(
        gradient: LinearGradient(
          begin: Alignment.topLeft,
          end: Alignment.topRight,
          colors: [AppColors.blueAppColor, AppColors.blueDarkAppColor ]
        )
      ),
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [            
          IconButton(
            color: AppColors.whiteAppColor,
            icon: ImageIcon(
              AssetImage("resources/images/arrow_left.png"),          
            ),
            onPressed: (){
              Navigator.of(context).pop();
            }
          ),
          Padding(
            padding: const EdgeInsets.only(left: 20, right: 120),
            child: Text(
              text,
              style: Styles.navigationHeaderText,
            ),
          ),
          IconButton(
            color: AppColors.whiteAppColor,
            icon: ImageIcon(
              AssetImage("resources/images/search.png"),          
            ),
            onPressed: (){}
          ),
          IconButton(
            color: AppColors.whiteAppColor,
            icon: ImageIcon(
              AssetImage("resources/images/filter.png"),          
            ),
            onPressed: (){}
          ),
        ],
      ),
    );
  }
}
