

import 'package:flutter/material.dart';

class DefaultBackground extends StatelessWidget {
  final String imageName;
  final Color color;

  const DefaultBackground({this.imageName, this.color});

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: imageName != null ? BoxDecoration(
        color: color,
        image: DecorationImage(fit: BoxFit.fitWidth, image: AssetImage(imageName))
      ) : 
      BoxDecoration(),
    );
  }
}