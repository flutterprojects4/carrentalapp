import 'package:CarRentalApp/common/app_colors/app_colors.dart';
import 'package:flutter/material.dart';

class Styles {

  static TextStyle firstHeaderText = TextStyle(
    color: AppColors.textBlackAppColor,
    fontSize: 32,
    fontWeight: FontWeight.bold    
  );

  static TextStyle secondHeaderText = TextStyle(
    color: AppColors.textGrayAppColor,
    fontSize: 32,
  );

  static TextStyle navigationHeaderText = TextStyle(
    color: AppColors.whiteAppColor,
    fontSize: 24,
  );

  static TextStyle firstButtonText = TextStyle(
    color: AppColors.whiteAppColor,
    fontFamily: 'Aleo',
    fontSize: 14,
    fontWeight: FontWeight.bold,
  );

  static TextStyle secondButtonText = TextStyle(
    color: AppColors.textGrayAppColor,
    fontFamily: 'Aleo',
    fontSize: 16,
    fontWeight: FontWeight.bold,
  );

  static TextStyle thirdButtonText = TextStyle(
    color: AppColors.textBlackAppColor,
    fontFamily: 'Aleo',
    fontSize: 16,
    fontWeight: FontWeight.bold,
  );

  static TextStyle carTitle = TextStyle(
    color: AppColors.textBlackAppColor,
    fontFamily: 'Aleo',
    fontSize: 20,
    fontWeight: FontWeight.bold,
  );

  static TextStyle carSubtitle = TextStyle(
    color: AppColors.textGrayAppColor,
    fontFamily: 'Aleo',
    fontSize: 12,
    fontWeight: FontWeight.bold,
  );

  static TextStyle carPrice = TextStyle(
    color: AppColors.orangeAppColor,
    fontFamily: 'Aleo',
    fontSize: 20,
    fontWeight: FontWeight.bold,
  );

  static TextStyle safeVehicle = TextStyle(
    color: AppColors.blueLightAppColor,
    fontFamily: 'Aleo',
    fontSize: 12,
    fontWeight: FontWeight.bold,
  );

  static TextStyle carInfo = TextStyle(
    color: AppColors.textBlackAppColor,
    fontFamily: 'Aleo',
    fontSize: 10,
    fontWeight: FontWeight.bold,
  );

}