import 'package:flutter/material.dart';

class AppColors {

  // App Colors

  static Color backgroundAppColor = Color(0xffFBFCFE);
  static Color blueAppColor = Color(0xff3C80F7);
  static Color blueDarkAppColor = Color(0xff1058D1);
  static Color blueLightAppColor = Color(0xff216DEE);

  static Color whiteAppColor = Color(0xffFFFFFF);
  static Color orangeAppColor = Color(0xffFE7B37);

  static Color textBlackAppColor = Color(0xff333333);
  static Color textGrayAppColor = Color(0xff888888);
}